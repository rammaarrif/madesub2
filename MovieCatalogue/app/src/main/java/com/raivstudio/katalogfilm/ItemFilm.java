package com.raivstudio.katalogfilm;

import org.json.JSONObject;

class ItemFilm {

    private String jdlfilm, sinopsis, tglfilm, ratefilm, gbrfilm, gbrbackdrop;

    public ItemFilm(JSONObject object) {
        try{
            String _judul       = object.getString("title");
            String _sinopsis    = object.getString("overview");
            String _tglrilis    = object.getString("release_date");
            String _rate        = object.getString("vote_average");
            String _poster      = object.getString("poster_path");
            String _backdrop    = object.getString("backdrop_path");

            this.jdlfilm    = _judul;
            this.sinopsis   = _sinopsis;
            this.tglfilm    = _tglrilis;
            this.ratefilm   = _rate;
            this.gbrfilm    = _poster;
            this.gbrbackdrop= _backdrop;
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public String getJdlfilm() {
        return jdlfilm;
    }

    public void setJdlfilm(String jdlfilm) {
        this.jdlfilm = jdlfilm;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getTglfilm() {
        return tglfilm;
    }

    public void setTglfilm(String tglfilm) {
        this.tglfilm = tglfilm;
    }

    public String getRatefilm() {
        return ratefilm;
    }

    public void setRatefilm(String ratefilm) {
        this.ratefilm = ratefilm;
    }

    public String getGbrfilm() {
        return gbrfilm;
    }

    public void setGbrfilm(String gbrfilm) {
        this.gbrfilm = gbrfilm;
    }

    public String getGbrbackdrop() {
        return gbrbackdrop;
    }

    public void setGbrbackdrop(String gbrbackdrop) {
        this.gbrbackdrop = gbrbackdrop;
    }
}
