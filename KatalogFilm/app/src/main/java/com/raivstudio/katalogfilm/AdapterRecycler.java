package com.raivstudio.katalogfilm;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.raivstudio.katalogfilm.search.DetailFilm;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterRecycler extends RecyclerView.Adapter<AdapterRecycler.ViewHolder>{

    private List<ItemFilm> itemFilm;
    private Context context;

    public AdapterRecycler(List<ItemFilm> itemFilm, Context context) {

        this.itemFilm = itemFilm;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie_recycler, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final ItemFilm _itemFilm = itemFilm.get(position);
        holder.jdl_film.setText(_itemFilm.getJdlfilm());
        holder.sin_film.setText(_itemFilm.getSinopsis());

        String tanggal = itemFilm.get(position).getTglfilm();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        try {
            Date date = format.parse(tanggal);

            SimpleDateFormat _formatbaru = new SimpleDateFormat("EEEE, dd - mm - yyyy");
            String _tanggalRilis = _formatbaru.format(date);
            holder.tgl_film.setText(_tanggalRilis);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Picasso.with(context).load("http://image.tmdb.org/t/p/w342/"+ itemFilm
                .get(position)
                .getGbrfilm())
                .placeholder(R.drawable.ic_image_black_24dp)
                .error(R.drawable.ic_broken_image_black_24dp)
                .into(holder.gb_film);

        holder.rv_film.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemFilm listFilm = itemFilm.get(position);
                Intent pindah = new Intent(context, DetailFilm.class);
                pindah.putExtra(DetailFilm.Extra_Judul, listFilm.getJdlfilm());
                pindah.putExtra(DetailFilm.Extra_tanggal, listFilm.getTglfilm());
                pindah.putExtra(DetailFilm.Extra_rating, listFilm.getRatefilm());
                pindah.putExtra(DetailFilm.Extra_Sinopsis, listFilm.getSinopsis());
                pindah.putExtra(DetailFilm.Extra_poster, listFilm.getGbrfilm());
                pindah.putExtra(DetailFilm.Extra_backdrop, listFilm.getGbrbackdrop());
                context.startActivity(pindah);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemFilm.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {

        TextView jdl_film;
        TextView sin_film;
        TextView tgl_film;
        ImageView gb_film;
        LinearLayout rv_film;

        public ViewHolder(View itemView) {
            super(itemView);

            jdl_film         = itemView.findViewById(R.id.tvJdl_recycler);
            sin_film         = itemView.findViewById(R.id.tvSin_recycler);
            tgl_film         = itemView.findViewById(R.id.tvTgl_recycler);
            gb_film          = itemView.findViewById(R.id.gbposter_recycler);
            rv_film          = itemView.findViewById(R.id.card_item);
        }
    }
}
