package com.raivstudio.katalogfilm;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raivstudio.katalogfilm.nowplaying.BerandaNowPlaying;
import com.raivstudio.katalogfilm.upcoming.BerandaUpcoming;

public class FragmentBeranda extends Fragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;

    public FragmentBeranda() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_beranda, container, false);
        viewPager = view.findViewById(R.id.containerMovie);
        viewPager.setAdapter(new SectionsPagerAdapter(getChildFragmentManager()));
        
        tabLayout = view.findViewById(R.id.TabLayout);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        return view;
    }
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            switch (position){
                case 0 :
                    return new BerandaNowPlaying();
                case 1:
                    return new BerandaUpcoming();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
