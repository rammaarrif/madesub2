package com.raivstudio.katalogfilm.nowplaying;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.raivstudio.katalogfilm.AdapterRecycler;
import com.raivstudio.katalogfilm.ItemFilm;
import com.raivstudio.katalogfilm.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BerandaNowPlaying extends Fragment {
private ArrayList<ItemFilm> listFilm = new ArrayList<>();
RecyclerView recyclerView;
RecyclerView.Adapter adapter;
private String Base_Url = "https://api.themoviedb.org/3/movie/now_playing?api_key=31c0a73972ca149daf5656b3830752e8&language=";

    public BerandaNowPlaying() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_beranda_now_playing, container, false);
        recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getUrl();

        return view;
    }

    private void getUrl() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.progress));
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url + getResources().getString(R.string.languange),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            progressDialog.dismiss();

                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray array = jsonObject.getJSONArray("result");

                                for (int i = 0 ; i < array.length() ; i++){

                                    JSONObject film = array.getJSONObject(i);
                                    ItemFilm _itemFilm = new ItemFilm(film);
                                    listFilm.add(_itemFilm);
                            }

                            adapter = new AdapterRecycler(listFilm, getActivity());
                                recyclerView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "error" + error.toString(), Toast.LENGTH_LONG).show();
                getUrl();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }
}
